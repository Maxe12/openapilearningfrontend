import {Component, OnInit} from '@angular/core';
import {Article, NewArticleService, NewsService} from '../../build/openapi';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'openapilearningfrontend';
  articles$: Article[];

 constructor(private newsService: NewsService) {}

  ngOnInit(): void {
    this.newsService.getNews().subscribe(news => {
      this.articles$ = news;
   });
  }

  postNewRandomArticle() {
   const article: Article = {
     title: 'Hallo',
     date: '2020-02-26',
     description: 'lol',
     imageUrl: 'eineUrl.com'
   };
   this.newsService.postNews(article).subscribe(res => {
     console.log(res);
   });
  }
}
